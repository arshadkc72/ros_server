
	const ReminderSchema = {
	  name : 'Reminder',
	   primaryKey: 'id',
	  properties : {
	      id :'string',
	    name : 'string',
	    done : 'bool',
	  }
	};

	module.exports = {
		ReminderSchemas : ReminderSchema,

	};
