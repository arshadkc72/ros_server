		const realmConfig = require('./RealmConfig');
		const Realm = require('realm');
		const mysql = require('mysql');
		const mysqlConfig = require('./mysqlConfig.js');
		Realm.Sync.setFeatureToken(realmConfig.FEATURE_TOKEN);

		var NOTIFIER_PATH = '^.*/remi4';


		var con = mysql.createConnection({
		  host: mysqlConfig.DBhost,
		  user: mysqlConfig.DBUserName,
		  password:  mysqlConfig.DBPassword,
		  database : mysqlConfig.DBSelected
		});
	function startMysqlConnection(){
console.log("MYSQL CONNECTION STARTING");
		con.connect(function(err){
			  if(err){
			    console.log(err);
			  }
			  else{
			    con.query("CREATE DATABASE IF NOT EXISTS " + mysqlConfig.DBSelected ,function(err,result){
			      if (err){
				console.log(err);
			      }
			      else{
			//id is combination of userid + reminder(index)
				 var tableQuery = "CREATE TABLE IF NOT EXISTS Reminder (id VARCHAR(255) NOT NULL,name VARCHAR(255),done bool,userID VARCHAR(255) NOT NULL,PRIMARY KEY (id) )";
				 con.query(tableQuery,function(err,result){
				   if (err){
				    console.log(err);
				   }
				   else{
				     console.log("Table created");
				   }
			});
				console.log("DATABASE is created success fully");
			      }
			    });
			    console.log("connected");
			  }
			});
	}

	// create the admin user
			var adminUser = Realm.Sync.User.adminUser(realmConfig.ADMIN_TOKEN,realmConfig.AUTH_URL);

		// The handleChange callback is called for every observed Realm file whenever it
		// has changes. It is called with a change event which contains the path, the Realm,
		// a version of the Realm from before the change, and indexes indication all objects
		// which were added, deconsted, or modified in this change

		function handleChange(changeEvent) {
		  // Extract the user ID from the virtual path, assuming that we're using
		  // a filter which only subscribes us to updates of user-scoped Realms.
		 	var matches = changeEvent.path.match(/^\/([0-9a-f]+)\/remi4/);
			if (matches != undefined && matches!= null && matches.length > 1) {
		  	 var userId = matches[1];
console.log("@@@@@@@@@@" + userId );
			console.log(changeEvent);
		  	var realm = changeEvent.realm;
		  	var coupons = realm.objects('Reminder');
		  	var couponIndexes = changeEvent.changes.Reminder.insertions;
			//INserteed data from DB or Other USERS
		    if (coupons[changeEvent.changes.Reminder.insertions] != undefined) {

			  const insertedTask = coupons[couponIndexes];
			  const val = (insertedTask.done == false) ? 0 : 1
			  console.log(val);
			//l
			  console.log(insertedTask.name.toString());

			  const selectQuery = "SELECT * FROM reminder WHERE id = '" + insertedTask.id + "'";

			  con.query(selectQuery,function(err,result){
			    if (err){
			      console.log("Select query error occured");
			    }
			    else if (result.length == 0) {
			    //  const reminde = realm.objects('Reminder').sorted('id', true)
			    //  var priID = (reminde.length  === 0 ) ? 1 : reminde[0].id ;
			      const insertQuery = "INSERT INTO reminder (name,done,id,userID) VALUES ('" + insertedTask.name +  "', '" + val + "', '" +insertedTask.id + "','" + userId + "' )";

				  console.log(insertQuery);
				  con.query(insertQuery,function(err,result){
				    if (err) console.log("insert error occured");
				    console.log(result);
				  });

		     	   }

			     });


			console.log(insertedTask);
		}
		else if (coupons[changeEvent.changes.Reminder.modifications] != undefined ){


		    var oldrealms = changeEvent.oldRealm;
		    var oldcoupons = oldrealms.objects('Reminder');
		    console.log(oldcoupons[changeEvent.changes.Reminder.modifications]);

		    var oldcouponIndexes = changeEvent.changes.Reminder.modifications;
		    const oldobject =  oldcoupons[oldcouponIndexes]

		    const insertedTask = coupons[changeEvent.changes.Reminder.modifications]
		    const val = (insertedTask.done == false) ? 0 : 1
		    console.log(oldobject);
		    const oldval = (oldobject.done == false) ? 0 : 1

		    if(oldcoupons[changeEvent.changes.Reminder.modifications] != coupons[changeEvent.changes.Reminder.modifications]){
		      console.log("update");
		      const selectQuery = "SELECT * FROM reminder WHERE id = '"+ insertedTask.id +"'";
		      con.query(selectQuery,function(err,result){
			if (err){
			  console.log("error");
			}
			else if (result.length > 0 ){
			  var updateQuery =  "UPDATE reminder SET name = '" + insertedTask.name + "', done = '" +  val + "' WHERE id = '"+  insertedTask.id + "' ";

			  console.log(updateQuery);

			  con.query(updateQuery,function(err,result){
			    if (err) console.log("update error occured");
			    console.log(result);
			  });
			}
			else{

		      const insertQuery = "INSERT INTO reminder (name,done,id,userID) VALUES ('" + insertedTask.name +  "', '" + val + "', '" + insertedTask.id + "' ,'"+ userId + "')";

				  console.log(insertQuery);
				  con.query(insertQuery,function(err,result){
				    if (err) console.log("insert error occured");
				    console.log(result);
				  });

			}

		      });

		    }


		}
		else if (changeEvent.oldRealm.objects('Reminder')[changeEvent.changes.Reminder.deconstions] != undefined ){

		  const object = changeEvent.oldRealm.objects('Reminder')[changeEvent.changes.Reminder.deconstions]
		  const oldval = (object.done == false) ? 0 : 1
		  const deconsteQuery =  "DELETE FROM  reminder  WHERE id  = '" + object.id + "'";

		  console.log(deconsteQuery);

		 	con.query(deconsteQuery,function(err,result){
		   	 if (err) console.log("delete query error occured");
		   	 console.log(result);
		  	});

		   }

}
		  }

	function RealmNotifier(){
console.log("listining started");
		// register the event handler callback
		Realm.Sync.addListener(realmConfig.SERVER_URL, adminUser, NOTIFIER_PATH, 'change', handleChange);

	}


	module.exports = {
	initss : startMysqlConnection,
	startRealm : RealmNotifier,
	ADMIN_USER :adminUser

	}
