	//# sourceMappingURL=index.js.map
	const express = require('express');
	const  bodyparser = require( 'body-parser');
	const app = express();
	const mysql = require('mysql');//const mysqlevents = require('mysql-events');
	//connect mysql db configuration
	const mysql_Config = require('./mysqlConfig.js');
	const ZongJi = require('zongji');
	const Realm = require('realm');
	const DBModule = require('./Modules');
	const RealmEventHandler = require('./RealmEvent.js');
	RealmEventHandler.initss();
	RealmEventHandler.startRealm();
	var zongji = new ZongJi({
	  host     : mysql_Config.DBhost,
	   port: mysql_Config.DBPort,
	  user     : mysql_Config.DBUserName,
	  password : mysql_Config.DBPassword,
	  // debug: true
	});

	//zongji used to check the db opreations
	function start(){
console.log("mysql EVENTS  started");

	zongji.on('binlog', function(evt) {
	//  console.log(evt)


	//  evt.getEventName() === 'writerows'
	//console.log(evt.tableMap[evt.tableId].parentSchema);
	  console.log(  evt.getEventName()  );

	if (evt.tableMap[evt.tableId].tableName === 'reminder') {
	  console.log("@@@@@@@@@@@@@@@");



	  if (evt.getEventName() === 'writerows'){

	      evt.rows.forEach(function (item) {

		    	const isDone  = (item.done == 0) ? false : true
			    console.log(isDone);
			    console.log(Realm.Sync.User.current);


			const realmURL = "realm://127.0.0.1:9080/"+item.userID+"/remi4"

		       Realm.open({ sync: { user: RealmEventHandler.ADMIN_USER,url: realmURL,},schema: [DBModule.ReminderSchemas]}).then(realm => {
const perd = "id = '"+ item.id + "'";
			 const remindesList = realm.objects('Reminder').filtered(perd)
			 console.log("UUUUuUUuuUUuuUU");
			 console.log(remindesList);
			  console.log("\\\\UUUUuUUuuUUuuUU");
			 if (remindesList.length == 0) {
			 realm.write(() => {
			  const myCar = realm.create('Reminder', {
			    name: item.name,
			    done: isDone,
			    id:item.id,
			  },true);
			});
			realm.close();

		      }
	       });

	});

	  }
	  else if (evt.getEventName() === 'updaterows'){

			 evt.rows.forEach(function (item) {
				 if (item["after"].userID != undefined){
	const realmURL = "realm://127.0.0.1:9080/"+item["after"].userID+"/remi4"
	       Realm.open({ sync: { user: RealmEventHandler.ADMIN_USER,url: realmURL,},schema: [DBModule.ReminderSchemas]}).then(realm => {

		 console.log(item["before"]);
		 console.log(item["after"]);
		 const isDone  = (item["after"].done == 0) ? false : true
		   console.log(isDone);
		   console.log(Realm.Sync.User.current);
		 realm.write(() => {
		  realm.create('Reminder', {
		    name: item["after"].name,
		    done: isDone,
		    id : item["after"].id
		  },true);
		});
				realm.close();
		    });
			}

	       });



	  }
	  else if (evt.getEventName() === 'deleterows'){

	    evt.rows.forEach(function (item) {

	     // console.log(item);
				if (item.userID != undefined){
	const realmURL = "realm://127.0.0.1:9080/"+item.userID+"/remi4"
	console.log(realmURL);
	    Realm.open({ sync: { user: RealmEventHandler.ADMIN_USER,url: realmURL,},schema: [DBModule.ReminderSchemas]}).then(realm => {
				console.log(item.id);
				const perd = "id = '"+ item.id + "'";
	      console.log(realm.objects('Reminder').filtered(perd));
		realm.write(() => {
		  realm.delete(realm.objects('Reminder').filtered(perd))
	       });
		realm.close();


	   });
	 }

	    });
	   console.log(evt.rows);
	  }
	  // else if (evt.getEventName() === 'tablemap'){
	  // console.log(evt);
	  // }

	  console.log("------------@@@@@@@@@@@@@@@\n");
	}

	});
	zongji.start({
	  includeEvents: ['tablemap', 'writerows', 'updaterows', 'deleterows']
	});

	}

	app.use(bodyparser.json());
	app.use(bodyparser.urlencoded({
	  extended : true
	}));


	console.log('Listening for Realm changes');

	console.log('started');

	process.on('SIGINT', function() {
	  Realm.logout();
	  console.log('Got SIGINT.');
	  zongji.stop();
	  process.exit();
	});

	module.exports = {
	startMysqlService : start
	}
