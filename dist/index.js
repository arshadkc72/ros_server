"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const realm_object_server_2 = require("realm-object-server");
const path = require("path");
const server = new realm_object_server_2.BasicServer();
const RealmObjectServer = require('realm-object-server')
const RealmConfig = require('./RealmConfig.js');
let usernamePasswordProvider = new realm_object_server_2.auth.PasswordAuthProvider({
    autoCreateAdminUser: true,
    saltLength: 32,
    iterations: 10000,
    keyLength: 512,
    digest: "sha512"
});
class MyAuthProvider extends RealmObjectServer.auth.AuthProvider{
  constructor(){
  super();
  this.name = 'myAuthProvider';
  }
  authenticateOrCreateUser(body) {
       const userId = body.userId;
       return this.service.createOrUpdateUser(
           userId,
           "myAuthProvider",
           false,
           null
       );
   }

}
//Add Facebook Authentication
const facebookprovider = new realm_object_server_2.auth.FacebookAuthProvider();
//google Authentication and add client id for your app

const googleProvider = new realm_object_server_2.auth.GoogleAuthProvider({
    clientId: '610176424813-qlf3b2tjaem63jc990847136vghm3496.apps.googleusercontent.com'
})
server.start({
    dataPath: path.join(__dirname, '../data'),
    authProviders: [usernamePasswordProvider,new MyAuthProvider(),facebookprovider,googleProvider],
}).then(() => {
  console.log(`Realm Object Server was started on ${server.address}`);
  //# sourceMappingURL=index.js.map
  //RealmConfig.FEATURE_TOKEN = require(path.resolve(server.config.dataPath, 'keys/admin.json')).ADMIN_TOKEN
  // const mysqlEventHandler = require('./MysqlEvents.js');
  // mysqlEventHandler.startMysqlService();

}).catch(err => {
    console.error(`Error starting Realm Object Server: ${err.message}`);
});

//# sourceMappingURL=index.js.map
