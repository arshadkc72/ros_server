import { BasicServer } from 'realm-object-server'
import * as path from 'path'

const server = new BasicServer()
const RealmObjectServer = require('realm-object-server')
//
let usernamePasswordProvider = new RealmObjectServer.auth.PasswordAuthProvider({
    autoCreateAdminUser: true,
    saltLength: 32,
    iterations: 10000,
    keyLength: 512,
    digest: "sha512"
});
//own Authenticates
class MyAuthProvider extends RealmObjectServer.auth.AuthProvider{
  constructor(){
  super();
  this.name = 'myAuthProvider';
  }
  authenticateOrCreateUser(body) {
       const userId = body.userId;
       return this.service.createOrUpdateUser(
           userId,
           "myAuthProvider",
           false,
           null
       );
   }

}
//Add Facebook Authentication
const facebookprovider = new RealmObjectServer.auth.FacebookAuthProvider();
//google Authentication and add client id for your app

const googleProvider = new RealmObjectServer.auth.GoogleAuthProvider({
    clientId: '610176424813-qlf3b2tjaem63jc990847136vghm3496.apps.googleusercontent.com'
})
//# sourceMappingURL=index.js.map
server.start({
        // This is the location where ROS will store its runtime data
        dataPath: path.join(__dirname, '../data'),
        authProviders: [ usernamePasswordProvider,new MyAuthProvider(),facebookprovider,googleProvider],
    }).then(() => {
    console.log(`Realm Object Server was started on ${server.address}`)
    const mysqlEventHandler = require('./MysqlEvents.js');
    mysqlEventHandler.startMysqlService();
    })
    .catch(err => {
        console.error(`Error starting Realm Object Server: ${err.message}`)
    })
